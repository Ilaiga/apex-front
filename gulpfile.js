var
    gulp         = require('gulp'),                  // Подключаем Gulp
    browserSync  = require('browser-sync'),          // Подключаем Browser Sync
    concat       = require('gulp-concat'),           // Подключаем gulp-concat (для конкатенации файлов)
    rename       = require('gulp-rename'),           // Подключаем библиотеку для переименования файлов
    del          = require('del'),                   // Подключаем библиотеку для удаления файлов и папок
    plumber      = require('gulp-plumber'),          // Отлавливаем ошибки
    size         = require('gulp-size'),             // Подключаем вывод размера файла

    sass         = require('gulp-sass'),             // Подключаем Sass
    autoprefixer = require('gulp-autoprefixer'),     // Подключаем автопрефиксер
    csso         = require('gulp-csso'),             // Подключаем пакет для минификации CSS
    combineMq    = require('gulp-combine-mq'),       // Группируем media запросы
    twig         = require('gulp-twig'),             // Подключаем шаблонизатор
    cheerio      = require('gulp-cheerio'),          // Манипуляции с HTML
    svgstore     = require('gulp-svgstore'),         // Подключение SVG-inline
    imagemin     = require('gulp-imagemin'),

    uglify       = require ('gulp-uglifyjs');        // Подключаем минификатор для JS

//Подключение twig
gulp.task('twig', function () {
    return gulp.src('src/twig/pages/*.twig')
        .pipe(plumber())
        .pipe(twig({
            data: {
                title: 'Gulp and Twig',
                benefits: [
                    'Fast',
                    'Flexible',
                    'Secure'
                ]
            }
        }))
        .pipe(gulp.dest('src/html'))
        .pipe(browserSync.reload({stream: true}))
});

// Настройка SASS
gulp.task('sass', function(){                      // Создаем таск SASS
    return gulp.src('src/sass/main.scss') // Берем источник
        .pipe(plumber())                           // отлавливаем ошибки при компиляции из SASS в CSS
        .pipe(sass().on('error', sass.logError))   // Преобразуем SASS в CSS
        .pipe(autoprefixer(
            ['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }
        ))                                        // Создаем префиксы
        .pipe(combineMq({beautify: false}))      // Группируем медиа запросы
        .pipe(size({                             // Вывод в консоль размер CSS
            showFiles: true
        }))
        .pipe(gulp.dest('src/css/'))
        .pipe(csso())                            // Сжимаем
        .pipe(rename({suffix: '.min'}))          // Добавляем суффикс .min
        .pipe(size({                             // Вывод в консоль размер CSS после минификации
            showFiles: true
        }))
        .pipe(size({                             // Вывод в консоль размер CSS после gzip
            showFiles: true,
            gzip: true
        }))
        .pipe(gulp.dest('src/css/'))              // Выгружаем минифицированный css
        .pipe(browserSync.reload({stream: true}));
});

//Инлайновый спрайт
gulp.task('sprite', function () {
    return gulp.src('src/img/icons-svg/*.svg')
        .pipe(cheerio({
            run: function ($) {
                $("svg").attr("style", "display: none");
                $("[fill]").removeAttr("fill");
                $("path").removeAttr("class");
                $("path").removeAttr("fill");
                $("path").removeAttr("style");
                $("style").remove();
            },
            parserOptions: {
                xmlMode: true
            }
        }))
        .pipe(svgstore({
            inlineSvg: true
        }))
        .pipe(rename('sprite-inline.svg'))
        .pipe(gulp.dest('src/img'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('svg-min', function () {
    return gulp.src('/img/sprite-inline.svg')
        .pipe(imagemin([imagemin.svgo()]))
        .pipe(gulp.dest('img'));
});

// Настройка Browser-sync (автоперезагрузка)
gulp.task('browser-sync', function() {       // Создаем таск browser-sync
    browserSync({                            // Выполняем browserSync
        server: {                            // Определяем параметры сервера
            baseDir: ["src/html", "src"]     // Директория по умолчанию
        },
        notify: false                        // Отключаем уведомления
    });
});

// Минификация JS
gulp.task('scripts', function() {
    return gulp.src([                           // Берем JS файлы для минификации
        'src/js/modules/*.js'
    ])
        .pipe(size({                      // Вывод в консоль размер JS
            showFiles: true
        }))
        .pipe(concat('main.js'))
        .pipe(uglify())                        // Сжимаем JS файл
        .pipe(size({                     // Вывод в консоль размер JS после минификации
            showFiles: true
        }))
        .pipe(size({                     // Вывод в консоль размер CSS после gzip
            showFiles: true,
            gzip: true
        }))
        .pipe(gulp.dest('src/js'))             // Выгружаем результат
        .pipe(browserSync.reload({stream: true}));
});

// Вся работа в фоне
gulp.task('watch', ['browser-sync', 'sprite', 'sass', 'scripts', 'twig', 'svg-min'], function() {
    gulp.watch('src/sass/**/*.scss', ['sass']);
    gulp.watch('src/twig/**/*.twig', ['twig']);
    gulp.watch('src/img/icons-svg/*.svg', ['sprite']);
    gulp.watch('src/js/*/*.js', ['scripts']);
});

// --> Перенос проекта в продакшн --> //
// Очистка папки build
gulp.task('clean', function() {
    return del.sync('build');
});



// Перенос файлов в продакшн
gulp.task('build', ['clean', 'sass', 'scripts'], function() {

    gulp.src([                        // Переносим библиотеки в продакшен
        'src/css/main.min.css'
    ])
        .pipe(gulp.dest('build/css'));

    gulp.src('src/fonts/**/*')        // Переносим шрифты в продакшен
        .pipe(gulp.dest('build/fonts'));

    gulp.src('src/js/**/*')           // Переносим скрипты в продакшен
        .pipe(gulp.dest('build/js'));

    gulp.src('src/*.html')            // Переносим HTML в продакшен
        .pipe(gulp.dest('build'));

    gulp.src('src/img/**/*.svg')      // Переносим SVG в продакшен
        .pipe(gulp.dest('build/img'));
});


// --> Перенос проекта в GitHubPages --> //
// Очистка папки docs
gulp.task('cleangit', function() {
    return del.sync('docs'); // Удаляем папку build перед сборкой
});


// Task по умолчнаию
gulp.task('default', ['watch']);