$(document).ready(function () {

    $('.intro__slider').owlCarousel ({
        items: 1,
        loop: true
    })

    $('.sales__slider').owlCarousel ({
        items: 4,
        loop: false,
        nav: true,
        margin: 18,
        dots: false,
        navText: ["<img class='arrow-left' src='img/simple-elements/arrow.svg'>", "<img src='img/simple-elements/arrow.svg'>"],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                loop:true
            },
            450:{
                items:2
            },
            768:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })

    $('.projects__slider').owlCarousel ({
        items: 3,
        loop: false,
        nav: true,
        margin: 20,
        dots: false,
        navText: ["<img class='arrow-left' src='img/simple-elements/arrow.svg'>", "<img src='img/simple-elements/arrow.svg'>"],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            576:{
                items:2
            },
            992:{
                items:3
            }
        }
    })

    $('.reviews__slider').owlCarousel ({
        items: 4,
        nav: true,
        loop: false,
        margin: 100,
        dots: false,
        navText: ["<img class='arrow-left' src='img/simple-elements/arrow.svg'>", "<img src='img/simple-elements/arrow.svg'>"],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                loop:true
            },
            450:{
                items:2
            },
            768:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })

    $('.clients__slider').owlCarousel ({
        items: 7,
        loop: true,
        nav: false,
        margin: 85,
        dots: false,
        navText: ["<img class='arrow-left' src='img/simple-elements/arrow.svg'>", "<img src='img/simple-elements/arrow.svg'>"],
        responsiveClass:true,
        responsive:{
            0:{
                items:2,
                margin: 50
            },
            400: {
                items: 3
            },
            576: {
                items: 4
            },
            768:{
                items:5
            },
            992:{
                items:6
            },
            1200:{
                items:7
            }
        }
    })

    $('.news__slider').owlCarousel({
        items: 4,
        nav: true,
        loop: false,
        margin: 20,
        dots: false,
        navText: ["<img class='arrow-left' src='img/simple-elements/arrow.svg'>", "<img src='img/simple-elements/arrow.svg'>"],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    })

    $("[data-fancybox]").fancybox({
        "padding" : 20,
        "frameWidth" : 700,
        "frameHeight" : 600,
        "overlayOpacity" : 0.8,
        btnTpl: {

            arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left prev " title="{{PREV}}">' +
                '<img src=\'img/simple-elements/arrow-left.svg\'>' +
                "</button>",

            arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right next" title="{{NEXT}}">' +
                '<img src=\'img/simple-elements/arrow-left.svg\'>' +
                "</button>"

        }

    });

});

$('.hamburger').on('click', function() {
    $('.header, .body, .header__menu-wrap').toggleClass("active");
});

$('.close').on('click', function () {
    $('.subheader .container').css("display", "none");
    $('.subheader').css("height", "0px");
    $('.subheader').css("transition", "height 0.3s ease");
});

$('.overlay').on('click', function () {
    $('.drop-list').removeClass("active");
    $('.arrow-down').removeClass("open");
    $('.overlay').addClass('hidden');
});

$('.header').on('click', function () {
    if(($('.drop-list-link').hasClass('clicked'))&&($('.header').hasClass('clicked'))) {
        $('.header').removeClass('clicked');
    }
    else
        if ($('.drop-list-link').hasClass('clicked')) {
            $('.drop-list').removeClass("active");
            $('.arrow-down').removeClass("open");
            $('.drop-list-link').removeClass('clicked');
            $('.overlay').addClass('hidden');
    }
});

$('.drop-list-link').on('click', function (event) {
    event.preventDefault();
    let bool=true;
    $('.drop-list-link').removeClass('clicked');
    $('.header').removeClass('clicked');
    let element = $(this).closest('.header-menu__list-item');
    if(element.find('.drop-list').hasClass('active'))
        bool=false;
    $('.drop-list').removeClass("active");
    $('.arrow-down').removeClass("open");
    if(bool) {
        element.find('.drop-list').addClass("active");
        element.find('.arrow-down').addClass("open");
    }
    $('.drop-list-link, .header').addClass('clicked');
    $('.header').addClass('clicked');
    $('.overlay').removeClass('hidden');
});

$('.header__address ul li').on('click', function () {
    let val = $(this).html();
    $('.header__address-link').html(val+"<button type=\"button\" class=\"arrow-down\"></button>");
    $('.cities-list').removeClass('active');
});